# Jaro-Winkler Exploratory Data

This repository contains sample (curated) data for the blog post
Enhancing Data Reconciliation with Jaro-Winkler Similarity in
Snowflake.

- `NCES.csv` contains a subset of official National Center for
  Educational Statistics listings of school district entities,
  constrained to the state of Virginia.

- `SUPP.csv` contains supplemental data with different district
  names, that is used as an example of the need for fuzzy
  similarity matching with Jaro-Winkler in Snowflake.

